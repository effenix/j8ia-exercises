// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice4;

// step 8: add class Teacher
// - fields: String name, String course
// - add constructor
// - encapsulate
// - add toString

public class Teacher {

    private String name;
	private String course;
	
	public Teacher(String name, String course) {
		this.name = name;
		this.course = course;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	@Override
	public String toString() {
    	return String.format("%s(%s, %s)", getClass().getSimpleName(), name, course);
	}
	
}
