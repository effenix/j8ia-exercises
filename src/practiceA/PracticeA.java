// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practiceA;

public class PracticeA {

	public static void main(String[] args) {
		System.out.println("Hello, PracticeA");
	}

}
