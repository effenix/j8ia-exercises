// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice7;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Practice7 {

	public static void main(String[] args) {
		// step 1: find any student that is good enough, in parallel
		// use the code from Practice5, step 11
		// what happens if you repeat the test multiple times?
		// add a delay if necessary (hint: use the transparent peek operation)
		System.out.println("\n--- 1: find any student that is good enough");
		Predicate<Student> goodEnough = student -> student.getExperience() >= 7;
		Data.students.parallelStream()
			.peek(Practice7::delay)
			.filter(goodEnough)
			.findAny()
			.ifPresent(System.out::println);

		// step 2: find the first student that is good enough
		// use the code from Practice5, step 12
		// is it the same student as the one found in step 1?
		System.out.println("\n--- 2: find the first student that is good enough");
		Data.students.parallelStream()
			.filter(goodEnough)
			.findFirst()
			.ifPresent(System.out::println);

		// step 4: sort a lot of random strings, sequentially
		// does the position of the sort operation matter?
		// measure the performance
		measure("4: sort a lot of random strings, sequentially", () -> {
			long count = Stream.generate(Practice7::randomString)
				.limit(3_000_000)
				.sorted()
				.count();
			System.out.printf("sorted %,d strings%n", count);
		});

		// step 5: sort a lot of random strings, in parallel
		// measure the performance
		measure("5: sort a lot of random strings, in parallel", () -> {
			long count = Stream.generate(Practice7::randomString)
				.parallel()
				.limit(3_000_000)
				.sorted()
				.count();
			System.out.printf("sorted %,d strings%n", count);
		});
		
		// step 6: run the ParallelStreams examples of the J8iA book, chapter 7
		// make sure you understand all the results
		System.out.println("\n--- 6: run the ParallelStreams examples of the J8iA book, chapter 7");
		lambdasinaction.chap7.ParallelStreamsHarness.main(new String[0]);
		
		// step 7: test the fork/join framework
		// generate random students 
		// find one with the maximum experience
		// do the same with a sequential and parallel streams
		// measure the results
		System.out.println("\n--- 7: test the fork/join framework");
		
		// generate random students
		int size = 50_000_000;
		List<Student> tooManyStudents = new ArrayList<>(size);
		System.out.printf("generating %,d random students", size);
		for (int i = 0; i < size; i++) {
			tooManyStudents.add(randomStudent());
			if (i % (size/10) == 0) {
				System.out.print(".");
			}
		}
		System.out.println();

		// find the best student, using fork/join
		measure("7a: find the best student, using fork/join", () -> {
	        ForkJoinPool pool = new ForkJoinPool();
	        FindBestStudent task = new FindBestStudent(tooManyStudents, 0, size - 1, size / 16);
	        Student bestStudent = pool.invoke(task);
	        System.out.println("Best student found: " + bestStudent);
		});

		// find the best student, using sequential stream
		measure("7b: find the best student, using sequential stream", () -> 
		tooManyStudents.stream()
			.max(Comparator.comparingInt(Student::getExperience))
			.ifPresent(System.out::println)
		);
		
		// find the best student, using parallel stream
		measure("7c: find the best student, using parallel stream", () -> 
		tooManyStudents.parallelStream()
			.max(Comparator.comparingInt(Student::getExperience))
			.ifPresent(System.out::println)
		);
	}
	
    // step 3: implement the "execute around" pattern in a central measure method
	// args: String message, Runnable task
	static void measure(String message, Runnable task) {
		System.out.printf("\n--- %s%n", message);
        long start = System.nanoTime();
		task.run();
        long duration = (System.nanoTime() - start) / 1_000_000;
        System.out.printf("--- duration: %,d ms%n", duration);
	}

	static void delay(Object o) {
		try {
			Thread.sleep(500);
		} 
		catch (InterruptedException e) {
		}
	}

	static String randomString() {
		Random random = new Random();
		int size = random.nextInt(16);
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i <= size; i++) {
			char c = (char) (97 + random.nextInt(26));
			sb.append(c);
		}
		return sb.toString();
	}

	static Student randomStudent() {
		return new Student(randomString(), new Random().nextInt(100));
	}
}
