// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice11;

import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

public class Practice11 {

	public static void main(String[] args) {
		// step 2: create an exam and let all students take it, sequentially
		// create an observer for the exam and an observer for all students
		// can we do something else in the mean time?
		System.out.println("\n--- 2: create an exam and let a student take it, sequentially");
		Exam ocaExam = new Exam("OCA Java Programmer I");
		BiConsumer<Exam, String> examObserver = (exam, message) -> System.out.println(message);
		ocaExam.addObserver(examObserver);
		BiConsumer<Student, String> studentObserver = (student, message) -> System.out.println(message);
		Data.students.forEach(student -> student.addObserver(studentObserver));
		Data.students.forEach(student -> student.takeExam(ocaExam));
		System.out.printf("--- While all students are doing the %s exam, we can do something else. Can we? No.%n", ocaExam.getSubject());
		
		// step 3: let all students take the exam, in parallel
		// can we do something else in the mean time?
		System.out.println("\n--- 3: let all students take the exam, in parallel");
		Data.students.parallelStream().forEach(student -> student.takeExam(ocaExam));
		System.out.printf("--- While all students are doing the %s exam, we can do something else. Can we? No.%n", ocaExam.getSubject());
		
		// step 5: let all students take the exam asynchronously, sequentially
		// can we do something else in the mean time?
		System.out.println("\n--- 5: let all students take the exam asynchronously, sequentially");
		Data.students.forEach(student -> student.takeExamAsync(ocaExam));
		System.out.printf("--- While all students are doing the %s exam, we can do something else. Can we? YES!%n", ocaExam.getSubject());
		
		// step 6: do the same thing, this time collecting all futures into a list and waiting for them to complete
		// (you'd better turn off the previous step, because it hasn't finished yet...)
		// print the result of the future when it has completed
		System.out.println("\n--- 6: do the same thing, this time collecting all futures and waiting for them to complete");
		List<CompletableFuture<Boolean>> futures = Data.students.stream()
			.map(student -> student.takeExamAsync(ocaExam))
			.collect(Collectors.toList());
		System.out.printf("--- While all students are doing the %s exam, we can do something else. Can we? YES!%n", ocaExam.getSubject());
		futures.forEach(future -> {
			boolean passed = future.join();
			System.out.printf("Student %s exam%n", passed ? "passed" : "failed");
		});
		
		// step 7: now collect all futures into a map with the student as the key
		// this has the advantage that we now know which student has passed or failed
		System.out.println("\n--- 7: now collect all futures into a map with the student as the key");
		Map<Student, CompletableFuture<Boolean>> futuresByStudent = Data.students.stream()
			.map(student -> new AbstractMap.SimpleEntry<Student, 
					CompletableFuture<Boolean>>(student, student.takeExamAsync(ocaExam)))
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		System.out.printf("--- While all students are doing the %s exam, we can do something else. Can we? YES!%n", ocaExam.getSubject());
		futuresByStudent.forEach((student, future) -> {
			boolean passed = future.join();
			System.out.printf("Student %s %s exam%n", student.getName(), passed ? "passed" : "failed");
		});

		// step 9: let each student study and after that take the exam
		// both studying and taking the exam should be done asynchronously
		// a student can take the exam as soon as (s)he finished studying, but not sooner
		// wait for all of them to finish
		System.out.println("\n--- 9: let each student learn and after that take the exam");
		@SuppressWarnings("rawtypes")
		CompletableFuture[] studyAndExam = Data.students.stream()
			.map(student -> student.studyAsync())
			.map(future -> future.thenCompose(student -> student.takeExamAsync(ocaExam)))
			.toArray(size -> new CompletableFuture[size]);
		System.out.printf("--- While all students are studying and taking the %s exam, we can do something else. Can we? YES!%n", ocaExam.getSubject());
		CompletableFuture.allOf(studyAndExam).join();
		System.out.println("All is well that ends well");
	}

}
