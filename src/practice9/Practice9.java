// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice9;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Practice9 {

	public static void main(String[] args) {
		// step 9: test if the changed Student observation still works
		// repeat Practice8 step 12
		System.out.println("\n--- 9: observing students, using instance method reference");
		Data.students.get(0).addObserver(Student::observed);
		Data.students.get(0).learn("programming in Swift");
		
		// step 11: create exam, add observer and let some students pass
		System.out.println("\n--- 11: create exam, add observer and let some students pass");
		Exam java8Exam = new Exam("Java 8");
		java8Exam.addObserver((exam, message) -> {
			System.out.println(message);
		});
		java8Exam.pass(Data.students.get(2));
		java8Exam.pass(Data.students.get(3));

		// step 12: sort students by experience and then reversed using Comparator static and default methods
		// (repeat from Practice3)
		System.out.println("\n--- 12: sort students by experience and then reversed using Comparator static and default methods");
		Collections.sort(Data.students, 
				Comparator.comparing(Student::getExperience)
						  .thenComparing(Comparator.comparing(Student::getName).reversed()));
		System.out.println(Data.students);

		// step 13: remove students using Collection default method
		System.out.println("\n--- 13: remove students using Collection default method");
		// why doesn't this work?
//		Data.students.removeIf(student -> student.getExperience() < 6);
		List<Student> studentsCopy = new ArrayList<>(Data.students);
		studentsCopy.removeIf(student -> student.getExperience() < 6);
		System.out.println(studentsCopy);

		// step 15: let the exam log a message
		System.out.println("\n--- 15: let the exam log a message");
		java8Exam.log("This is an exam");

		// step 19: let the exam log another message
		// try to explicitly call the log method from the Logger interface
		System.out.println("\n--- 19: let the exam log another message");
		((Logger)java8Exam).log("This is still an exam");

		// step 21: let the Logger interface log a message
		System.out.println("\n--- 21: let the Logger interface log a message");
		Logger.logMessage("This is a static logger");
	}

}
