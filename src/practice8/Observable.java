// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice8;

import java.util.function.BiConsumer;

// step 1: add interface Observable
// method: addObserver(BiConsumer)
// the consumer must accept an Observable and a message String
public interface Observable<T> {
	public void addObserver(BiConsumer<T, String> consumer);
}
