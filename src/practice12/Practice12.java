// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice12;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.TemporalAdjusters;
import java.util.Arrays;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Practice12 {

	public static void main(String[] args) {
		printZoneIds();
		Map<String, ZoneId> zoneMap = mapZoneIds();
		flying(zoneMap.get("Brussels"), zoneMap.get("Tokyo"), Duration.ofHours(11).plusMinutes(25));
		passingTime();
	}

	// step 1: write and call a method that does the following:
	// stream all names of all available zone id's
	// filter on Brussels or Tokyo
	// print the resulting names
	private static void printZoneIds() {
		System.out.println("\n--- 1: printZoneIds");
	    Set<String> zones = ZoneId.getAvailableZoneIds();
	    Predicate<String> brussels = zone -> zone.toLowerCase().contains("brussels");
	    Predicate<String> tokyo = zone -> zone.toLowerCase().contains("tokyo");
	    zones.stream()
	    	.filter(brussels.or(tokyo))
	    	.forEach(System.out::println);
	}

	// step 2: write and call a method that does the following:
	// stream all names of all available zone id's
	// collect them in a map, key: city name, value: zone id
	// return the map
	// hint: a zone id is a string of names separated by a /; the city is the last name
	// be aware: there will be duplicate keys, but the API has a solution for that...
	private static Map<String, ZoneId> mapZoneIds() {
		System.out.println("\n--- 2: mapZoneIds");
	    Set<String> zones = ZoneId.getAvailableZoneIds();
	    zones.stream()
	    	.map(zone -> Arrays.asList(zone.split("/")))
	    	.forEach(System.out::println);
	    Map<String, ZoneId> map = zones.stream()
	    	.collect(Collectors.toMap(
	    			zone -> {
	    				String[] parts = zone.split("/");
	    				return parts[parts.length - 1];
	    			}, 
	    			zone -> ZoneId.of(zone),
	    			(v1, v2) -> v1));
	    return map;
}

	// step 3: write and call a method that does the following:
	// arguments: ZoneId from, ZoneId to, Duration flying time
	// a plane needs to leave from Brussels on the last Saturday of this month, at 16:00
	// it's flying to Tokyo, flying time 11:10 hours
	// use the map from the previous step to get the zone ids
	// print the date & time of departure in long local format
	// what date & time is it in Tokyo when we arrive (print it in local and in Japanese format)?
	private static void flying(ZoneId from, ZoneId to, Duration flyingTime) {
		System.out.println("\n--- 3: flying");
		LocalDate now = LocalDate.now();
		LocalDate lastSaturdayOfTheMonth = now.with(TemporalAdjusters.lastInMonth(DayOfWeek.SATURDAY));
		System.out.println(lastSaturdayOfTheMonth);
		LocalTime fourOClock = LocalTime.of(16, 0);
		LocalDateTime saturdayAtFour = LocalDateTime.of(lastSaturdayOfTheMonth, fourOClock);
		System.out.println(saturdayAtFour);
		ZonedDateTime departure = ZonedDateTime.of(saturdayAtFour, from);
		System.out.println("Departure: " + departure.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.FULL)));
		ZonedDateTime arrivalFrom = departure.plus(flyingTime);
		System.out.println("Arrival: " + arrivalFrom.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.FULL)));
		ZonedDateTime arrivalTo = departure.plus(flyingTime).withZoneSameInstant(to);
		System.out.println("Arrival: " + arrivalTo.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.FULL)));
		System.out.println("Arrival: " + arrivalTo.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.FULL).withLocale(Locale.JAPAN)));
	}

	// step 4: write and call a method that does the following:
	// show the difference between LocalDateTime and ZonedDateTime,
	// and between Period and Duration
	// start by creating a LocalDateTime of 2019-03-30, noon, and print it (without any formatting)
	// add a Period of 1 day and print the result
	// instead, add a Duration of 1 day and print the result
	// repeat these 3 steps for a ZonedDateTime in Brussels
	// what are the differences?
	private static void passingTime() {
		System.out.println("\n--- 4: passingTime");
		// use a LocalDateTime to do calculations
		LocalDateTime winter = LocalDateTime.of(2019, Month.MARCH, 30, 12, 0);
		System.out.println("winter: " + winter);
		LocalDateTime spring = winter.plus(Period.ofDays(1));
		System.out.println("spring: " + spring);
		LocalDateTime springTime = winter.plus(Duration.ofDays(1));
		System.out.println("springTime: " + springTime);
		// use a LocalDateTime to do calculations
		ZoneId brussels = ZoneId.of("Europe/Brussels");
		ZonedDateTime winterInBrussels = ZonedDateTime.of(winter, brussels);
		System.out.println("winterInBrussels: " + winterInBrussels);
		ZonedDateTime springInBrussels = winterInBrussels.plus(Period.ofDays(1));
		System.out.println("springInBrussels: " + springInBrussels);
		ZonedDateTime springTimeInBrussels = winterInBrussels.plus(Duration.ofDays(1));
		System.out.println("springTimeInBrussels: " + springTimeInBrussels);
	}

}
