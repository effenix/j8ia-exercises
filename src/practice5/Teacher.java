// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice5;

import java.util.ArrayList;
import java.util.List;

public class Teacher {

	private String name;
	private String course;
	// step 1: add a list of Students, initially empty
	private List<Student> students = new ArrayList<>();
	
	public Teacher(String name, String course) {
		this.name = name;
		this.course = course;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}
	
	// step 2: 
	// - add getter & adder for the students
	public List<Student> getStudents() {
		return students;
	}
	
	public void addStudent(Student student) {
		students.add(student);
	}

	@Override
	public String toString() {
    	return String.format("%s(%s, %s)", getClass().getSimpleName(), name, course);
	}
	
}
