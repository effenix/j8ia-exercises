// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice6;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.maxBy;
import static java.util.stream.Collectors.partitioningBy;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class Practice6 {

	public static void main(String[] args) {

		// step 1: group all students by their experience, imperative style
		// use a map where the experience is the key, and the list of related students is the value
		System.out.println("\n--- 1: group all students by their experience, imperative style");
		Map<Integer, List<Student>> studentsByExperience1 = new HashMap<>();
		for (Student student: Data.students) {
			int experience = student.getExperience();
			List<Student> students = studentsByExperience1.get(experience);
			if (students == null) {
				students = new ArrayList<>();
				studentsByExperience1.put(experience, students);
			}
			students.add(student);
		}
		System.out.println(studentsByExperience1);

		// step 2: group all students by their experience, declarative style
		System.out.println("\n--- 2: group all students by their experience, declarative style");
		Map<Integer, List<Student>> studentsByExperience2 = Data.students.stream()
			.collect(groupingBy(Student::getExperience));
		System.out.println(studentsByExperience2);

		// step 3: group all students by their teacher, declarative style
		System.out.println("\n--- 3: group all students by their teacher, declarative style");
		Map<Teacher, List<Student>> studentsByTeacher = Data.students.stream()
			.collect(groupingBy(Student::getTeacher));
		System.out.println(studentsByTeacher);

		// step 4: for each teacher, count the number of students
		// 4th try: use grouping
		// use a map where the teacher is the key, and the count is the value
		// use two-level grouping
		System.out.println("\n--- 4: for each teacher, count the number of students, using grouping");
		Map<Teacher, Long> studentCountByTeacher = Data.students.stream()
			.collect(groupingBy(Student::getTeacher, counting()));
		System.out.println(studentCountByTeacher);
		
		// step 5: for each teacher, find the best student
		// 2nd try: use collectors
		// use a map where the teacher is the key, and the student is the value
		// use two-level grouping
		System.out.println("\n--- 5: for each teacher, find the best student");
		Map<Teacher, Student> bestStudents = Data.students.stream()
			.collect(groupingBy(
				Student::getTeacher,
				collectingAndThen(
					maxBy(Comparator.comparingInt(Student::getExperience)),
					Optional::get)
				)
			);
		System.out.println(bestStudents);

		// step 6: partition the students by bad and good
		System.out.println("\n--- 6: partition the students by bad and good");
		Map<Boolean, List<Student>> studentsWithExperience = Data.students.stream()
			.collect(partitioningBy(student -> student.getExperience() > 6));
		System.out.println(studentsWithExperience);

		// step 7: for each teacher with students, join the names of the students
		// 1st try: use collector
		System.out.println("\n--- 7: for each teacher with students, join the names of the students, using collector");
		Data.teachers.stream()
			.filter(teacher -> teacher.getStudents().size() > 0)
			.forEach(teacher -> {
				String names = teacher.getStudents().stream()
					.map(Student::getName)
					.collect(joining(", "));
				System.out.printf("%s's students: %s%n", teacher.getName(), names);
		});
		
		// step 8: for each teacher with students, join the names of the students
		// 2nd try: use grouping
		System.out.println("\n--- 8: for each teacher with students, join the names of the students, using grouping");
		Map<Teacher, String> studentNamesByTeacher = Data.students.stream()
			.collect(groupingBy(
				Student::getTeacher,
				mapping(Student::getName, joining(", "))
				)
			);
		System.out.println(studentNamesByTeacher);
	}
}
