// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice10;

import java.time.LocalDate;
import java.util.function.BiConsumer;

public class Practice10 {

	public static void main(String[] args) {
		// step 2: create an exam and let a student pass
		// create an observer for the exam to see the result
		System.out.println("\n--- 2: create an exam and let a student pass");
		Exam java8Exam = new Exam("Java 8");
		BiConsumer<Exam, String> examObserver = (exam, message) -> System.out.println(message);
		java8Exam.addObserver(examObserver);
		java8Exam.pass(Data.students.get(0));

		// step 5: create an exam with a final date before now and let a student pass
		// hint: use LocalDate.of()
		// create an observer for the exam to see the result
		System.out.println("\n--- 4: create an exam with a final date before now and let a student pass");
		Exam java7Exam = new Exam("Java 7", LocalDate.of(2019, 1, 1));
		java7Exam.addObserver(examObserver);
		java7Exam.pass(Data.students.get(1));
		
		// step 6: create an exam with a final date after now and let a student pass
		// create an observer for the exam to see the result
		System.out.println("\n--- 5: create an exam with a final date after now and let a student pass");
		Exam java9Exam = new Exam("Java 9", LocalDate.of(2020, 1, 1));
		java9Exam.addObserver(examObserver);
		java9Exam.pass(Data.students.get(2));
		
		// step 13: for all students. get the finalDate of their exam
		// use flatMap
		System.out.println("\n--- 12: for all students. get the finalDate of their exam");
		Data.students.stream()
			.map(Student::getExam)
			.forEach(optionalExam -> {
				optionalExam.flatMap(Exam::getFinalDate)
							.ifPresent(System.out::println);
			});
	}
}
