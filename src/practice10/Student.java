// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice10;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;

public class Student implements Observable<Student> {
	
    private String name;
    private int experience;
    private Teacher teacher;
    List<BiConsumer<Student, String>> observers = new ArrayList<>();
    // step 10: add optional Exam field
    Exam exam;
    

    public Student(String name, int experience) {
        this.name = name;
        this.experience = experience;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
		teacher.addStudent(this);
	}

    @Override
    public String toString() {
    	return String.format("%s(%s, %d)", getClass().getSimpleName(), name, experience);
    }
    
    public static int compareByName(Student s1, Student s2) {
		return s1.getName().compareTo(s2.getName());
    }
    
    public static int compareByExperience(Student s1, Student s2) {
		return s1.getExperience() - s2.getExperience();
    }
    
    public int compareToName(Student s2) {
		return name.compareTo(s2.getName());
    }
    
    public int compareToExperience(Student s2) {
		return experience - s2.getExperience();
    }

	@Override
	public List<BiConsumer<Student, String>> getObservers() {
		return observers;
	}

	public void learn(String task) {
		experience++;
		notifyObservers("learned " + task);
	}

	public void observed(String message) {
		System.out.printf("%s finally %s%n", name, message);
	}

	// step 11: add setters & getters for exam
	// do not use optional arg for setter
	public Optional<Exam> getExam() {
		return Optional.ofNullable(exam);
	}

	public void setExam(Exam exam) {
		this.exam = exam;
	}
	
}
