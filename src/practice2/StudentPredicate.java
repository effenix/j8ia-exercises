// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice2;

// step 11:
// - Create interface StudentPredicate
// - method: test

public interface StudentPredicate {
    boolean test(Student student);
}
