// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author fred
 */
public class Practice2 {

    public static void main(String[] args) {
    	
        // step 3: create Practice objects
        Practice2 p2 = new Practice2();
        
        // step 4: print original list
        // hint: use a static print method to add some comment
        print("all students", Data.students);
        
        // step 6: filter good students, fixed criterion
        print("good students", p2.goodStudents());
        
        // step 8: filter the best students, parameterized
        print("the best students, parameterized", p2.studentsWithExperience(8));
        
        // step 10: filter students named George, parameterized
        print("students named George, parameterized", p2.studentsWithName("George"));
        
        // step 15: filter the best students, by predicate, using class
        print("the best students, by predicate", p2.studentsByPredicate(new StudentExperiencePredicate()));
        
        // step 16: filter students named George, by predicate, using class
        print("students named George, by predicate", p2.studentsByPredicate(new StudentNamePredicate()));
        
        // step 17: filter the best students, by predicate, using anonymous inner class
        print("the best students, by predicate, with anonymous inner class", p2.studentsByPredicate(new StudentPredicate() {
            @Override
            public boolean test(Student student) {
                return student.getExperience() > 7;
            }
        }));
        
        // step 18: filter students named George, by predicate, using anonymous inner class
        print("students named George, by predicate, with anonymous inner class", p2.studentsByPredicate(new StudentPredicate() {
            @Override
            public boolean test(Student student) {
                return student.getName().contains("George");
            }
        }));
        
        // step 19: filter the best students, by predicate, using lambda expression
        print("the best students, by predicate, using lambda expression", p2.studentsByPredicate((Student student) -> student.getExperience() > 7));
        
        // step 20: filter students named George, by predicate, using lambda expression
        print("students named George, by predicate, using lambda expression", p2.studentsByPredicate((Student student) -> student.getName().contains("George")));
        
        // step 23 (bonus): generic filter students named George, by predicate, using lambda expression
        print("students named George, generic, using lambda expression", filter(Data.students, (Student student) -> student.getName().contains("George")));
        
        // step 24 (bonus): generic filter Strings
        List<String> names = Arrays.asList(new String[] {"John", "Paul", "George", "Ringo"});
        print("names longer than 4 characters, generic", filter(names, (String name) -> name.length() > 4));
    }

    private static void print(String comment, List<? extends Object> list) {
        System.out.printf("%n--- %s%n", comment);
        System.out.println(list);
    }

    // step 5: filter students, fixed criterion
    private List<Student> goodStudents() {
        List<Student> result = new ArrayList<>();
        for (Student student: Data.students) {
            if (student.getExperience() > 7) {
                result.add(student);
            }
        }
        return result;
    }

    // step 7: filter students, parameterized criterion
    private List<Student> studentsWithExperience(int experience) {
        List<Student> result = new ArrayList<>();
        for (Student student: Data.students) {
            if (student.getExperience() > experience) {
                result.add(student);
            }
        }
        return result;
    }

    // step 9: filter students, parameterized criterion
    private List<Student> studentsWithName(String name) {
        List<Student> result = new ArrayList<>();
        for (Student student: Data.students) {
            if (student.getName().contains(name)) {
                result.add(student);
            }
        }
        return result;
    }

    // step 14: filter students, by predicate
    private List<Student> studentsByPredicate(StudentPredicate predicate) {
        List<Student> result = new ArrayList<>();
        for (Student student: Data.students) {
            if (predicate.test(student)) {
                result.add(student);
            }
        }
        return result;
    }

    // step 22 (bonus): generic filter, by predicate
    static private <T> List<T> filter(List<T> list, Predicate<T> predicate) {
        List<T> result = new ArrayList<>();
        for (T t: list) {
            if (predicate.test(t)) {
                result.add(t);
            }
        }
        return result;
    }

}
