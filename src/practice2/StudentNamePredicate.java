// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice2;

// step 13:
// - Create class StudentNamePredicate
// - implements StudentPredicate
public class StudentNamePredicate implements StudentPredicate {

    @Override
    public boolean test(Student student) {
        return student.getName().contains("George");
    }
    
}
