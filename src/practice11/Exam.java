// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice11;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;

public class Exam implements Observable<Exam> {
	private String subject;
    List<BiConsumer<Exam, String>> observers = new ArrayList<>();
    Optional<LocalDate> finalDate = Optional.empty();

	public Exam(String subject) {
		this.subject = subject;
	}
	
	public Exam(String subject, LocalDate finalDate) {
		this(subject);
		this.finalDate = Optional.of(finalDate);
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Override
	public String toString() {
    	return String.format("%s(%s)", getClass().getSimpleName(), subject);
	}
	
	@Override
	public List<BiConsumer<Exam, String>> getObservers() {
		return observers;
	}
	
	public void pass(Student student) {
		if (finalDate.isPresent() && finalDate.get().isBefore(LocalDate.now())) {
			System.out.println("The exam is no longer available");
			return;
		}
		student.setExam(this);
		notifyObservers(student.getName() + " passed exam " + subject);
	}
	
	public Optional<LocalDate> getFinalDate() {
		return finalDate;
	}
}
