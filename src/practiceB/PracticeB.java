// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practiceB;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

public class PracticeB {

	public static void main(String[] args) {
		System.out.println("Hello, PracticeB");
		
		// step 1: Arrays
		// set all elements of a Student array to a random student
		// use "asList" to turn the array into a List
		// print all students
		System.out.println("\n--- 1: Arrays");
		Student[] students = new Student[10];
		Arrays.setAll(students, (index) -> randomStudent());
		List<Student> studentList = Arrays.asList(students);
		studentList.forEach(System.out::println);
		
		// step 2: Collection
		// remove students with an experience less than half the maximum 
		// explain what's wrong and fix it
		System.out.println("\n--- 2: Collection");
		System.out.printf("Generated %d students%n", studentList.size());
//		studentList.removeIf(student -> student.getExperience() < 50);
		List<Student> shortList = new ArrayList<>(studentList);
		shortList.removeIf(student -> student.getExperience() < 50);
		System.out.printf("Left %d students%n", shortList.size());

		// step 3: Map
		// create a map of all students by teacher
		// if a teacher is not in the map, add that teacher, having the random students from above
		// print something about each teacher-student pair
		System.out.println("\n--- 3: Map");
		Map<Teacher, List<Student>> studentsByTeacher = Data.students.stream()
			.collect(Collectors.groupingBy(Student::getTeacher));
		Data.teachers.forEach(teacher -> 
			studentsByTeacher.computeIfAbsent(teacher, teacherKey -> 
				{
					shortList.forEach(student -> student.setTeacher(teacherKey));
					return shortList;
				}));
		studentsByTeacher.forEach((teacher, teacherStudents) -> 
				teacherStudents.forEach(student -> System.out.printf("%s teaches %s%n", student.getTeacher().getName(), student.getName())));
		
		// step 4: Files
		// list all files in the "src" directory
		// recursively walk the "src" directory
		// find all files under the "src" directory that belong to this practice
		System.out.println("\n--- 4: Files");
		Path path = Paths.get("src");
		try {
			System.out.println("\n--- list:");
			Files.list(path).forEach(System.out::println);
			System.out.println("\n--- walk:");
			Files.walk(path).forEach(System.out::println);
			System.out.println("\n--- find:");
			Files.find(path, 10, (p, attr) -> p.startsWith("src/practiceB")).forEach(System.out::println);
		} catch (IOException x) {
			x.printStackTrace();
		}
	}

	static String randomString() {
		Random random = new Random();
		int size = random.nextInt(16);
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i <= size; i++) {
			char c = (char) (97 + random.nextInt(26));
			sb.append(c);
		}
		return sb.toString();
	}

	static Student randomStudent() {
		return new Student(randomString(), new Random().nextInt(100));
	}

}
