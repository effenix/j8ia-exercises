// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice3;

public class Student {
	
    private String name;
    private int experience;

    public Student(String name, int experience) {
        this.name = name;
        this.experience = experience;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
    	return String.format("%s(%s, %d)", getClass().getSimpleName(), name, experience);
    }
    
    // step 5: add static method for comparing 2 students by name
    public static int compareByName(Student s1, Student s2) {
		return s1.getName().compareTo(s2.getName());
    }
    
    // step 7: add static method for comparing 2 students by experience
    public static int compareByExperience(Student s1, Student s2) {
		return s1.getExperience() - s2.getExperience();
    }
    
    // step 9: add instance method for comparing to another student by name
    // first, let the name be "compareByName" and try to explain the error you get
    public int compareToName(Student s2) {
		return name.compareTo(s2.getName());
    }
    
    // step 11: add instance method for comparing to another student by experience
    public int compareToExperience(Student s2) {
		return experience - s2.getExperience();
    }
}
