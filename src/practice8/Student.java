// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice8;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

// step 2: implement Observable
public class Student implements Observable<Student> {
	
    private String name;
    private int experience;
    private Teacher teacher;
    // step 3: add empty list of Consumers
    List<BiConsumer<Student, String>> observers = new ArrayList<>();
    

    public Student(String name, int experience) {
        this.name = name;
        this.experience = experience;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
		teacher.addStudent(this);
	}

    @Override
    public String toString() {
    	return String.format("%s(%s, %d)", getClass().getSimpleName(), name, experience);
    }
    
    public static int compareByName(Student s1, Student s2) {
		return s1.getName().compareTo(s2.getName());
    }
    
    public static int compareByExperience(Student s1, Student s2) {
		return s1.getExperience() - s2.getExperience();
    }
    
    public int compareToName(Student s2) {
		return name.compareTo(s2.getName());
    }
    
    public int compareToExperience(Student s2) {
		return experience - s2.getExperience();
    }

    // step 4: implement Observable method 
    // add consumer to list
	@Override
	public void addObserver(BiConsumer<Student, String> consumer) {
		observers.add(consumer);
	}
	
	// step 5: add notifyObservers method
	// for each observer, call the consumers method
	public void notifyObservers(String message) {
		observers.forEach(observer -> observer.accept(this, message));
	}
	
	// step 6: add method learn
	// arg: String task
	// increment experience
	// notify observers
	public void learn(String task) {
		experience++;
		notifyObservers("learned " + task);
	}

	// step 11: create instance method to be used as a reference to BiConsumer.accept
	public void observed(String message) {
		System.out.printf("%s finally %s%n", name, message);
//		throw new RuntimeException("bang!");
	}
}
