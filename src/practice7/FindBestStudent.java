// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice7;

import java.util.List;
import java.util.concurrent.RecursiveTask;

public class FindBestStudent extends RecursiveTask<Student> {

	private static final long serialVersionUID = 1L;
	private final int threshold;
    private final List<Student> students;
    private int start;
    private int end;

    public FindBestStudent(List<Student> students, int start, int end, int threshold) {
        this.threshold = threshold;
        this.students = students;
        this.start = start;
        this.end = end;
    }

	@Override
	protected Student compute() {
		int length = end - start;
        if (length < threshold) {
        	// find the best student in our part of the list
            Student best = null;
            for (int i = start; i <= end; i++) {
            	Student next = students.get(i);
            	if (best == null || next.getExperience() > best.getExperience()) {
                    best = next;
                }
            }
            return best;
        } else {
        	// recursively start 2 new tasks each with it's own half of our list
            int midway = start + length / 2;
            // start the first task in a new thread
            FindBestStudent a1 = new FindBestStudent(students, start, midway, threshold);
            a1.fork();
            // start the second task in the current thread
            FindBestStudent a2 = new FindBestStudent(students, midway + 1, end, threshold); 
            Student first = a2.compute();
            // wait for the first task to finish
            Student second = a1.join();
            return first.getExperience() > second.getExperience() ? first : second;
        }
	}

}
