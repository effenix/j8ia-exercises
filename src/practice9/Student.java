// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice9;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

public class Student implements Observable<Student> {
	
    private String name;
    private int experience;
    private Teacher teacher;
    List<BiConsumer<Student, String>> observers = new ArrayList<>();
    

    public Student(String name, int experience) {
        this.name = name;
        this.experience = experience;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
		teacher.addStudent(this);
	}

    @Override
    public String toString() {
    	return String.format("%s(%s, %d)", getClass().getSimpleName(), name, experience);
    }
    
    public static int compareByName(Student s1, Student s2) {
		return s1.getName().compareTo(s2.getName());
    }
    
    public static int compareByExperience(Student s1, Student s2) {
		return s1.getExperience() - s2.getExperience();
    }
    
    public int compareToName(Student s2) {
		return name.compareTo(s2.getName());
    }
    
    public int compareToExperience(Student s2) {
		return experience - s2.getExperience();
    }

	// step 5: remove the methods that are now part of the Observable interface
    
	// step 6: implement the getObservers method of the Observable interface
	@Override
	public List<BiConsumer<Student, String>> getObservers() {
		return observers;
	}

	public void learn(String task) {
		experience++;
		notifyObservers("learned " + task);
	}

	public void observed(String message) {
		System.out.printf("%s finally %s%n", name, message);
	}
}
