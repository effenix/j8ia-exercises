// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice8;

import java.util.Comparator;
import java.util.function.BiConsumer;

public class Practice8 {

	public static void main(String[] args) {
		
		// step 7: add observers to some or all students and let them learn
		// use anonymous inner class for the BiConsumer
		System.out.println("\n--- 7: observing students, using anonymous inner class");
		BiConsumer<Student, String> anonymousObserver = new BiConsumer<Student, String>() {
			@Override
			public void accept(Student student, String message) {
				System.out.printf("message from %s: I %s%n", student, message);
			}
		};
		Data.students.get(0).addObserver(anonymousObserver);
		Data.students.get(0).learn("using anonymous inner class, even in Java 8");
		
		// step 8: add observers to some or all students and let them learn
		// use lambda expression for the BiConsumer
		System.out.println("\n--- 8: observing students, using lambda expression");
		BiConsumer<Student, String> lambdaObserver = (student, message) -> System.out.printf("%s %s%n", student, message);
		Data.students.get(0).addObserver(lambdaObserver);
		Data.students.get(0).learn("the observers design pattern in Java 8");
		Data.students.get(0).learn("being happy");
		Data.teachers.forEach(
			teacher -> teacher.getStudents().forEach(
				student -> student.addObserver(lambdaObserver)
			)
		);
		Data.students.get(0).learn("even more");
		Data.teachers.get(1).getStudents().get(1).learn("drinking coffee");
		
		// step 10: add observers to some or all students and let them learn
		// use static method reference for the BiConsumer
		System.out.println("\n--- 10: observing students, using static method reference");
		Data.students.get(0).addObserver(Practice8::methodObserver);
		Data.students.get(0).learn("to think");
		
		// step 12: add observers to some or all students and let them learn
		// use instance method reference for the BiConsumer
		System.out.println("\n--- 12: observing students, using instance method reference");
		Data.students.get(0).addObserver(Student::observed);
		Data.students.get(0).learn("to wonder");
		
		// step 13: throw an unchecked exception in a lambda expression
		System.out.println("\n--- 13: throw an unchecked exception in a lambda expression");
		Teacher fred = new Teacher("Fred", "Java");
		Student unknown = new Student(null, 0);
		unknown.setTeacher(fred);
//		fred.getStudents().stream().forEach(student -> System.out.println(student.getName().length()));
		
		// step 14: throw an unchecked exception in Practice8::methodObserver or in Student::observed
		System.out.println("\n--- 14: throw an unchecked exception in Practice8::methodObserver or in Student::observed");
		Data.students.get(0).learn("to crash");
		
		// step 15: insert peek operations in a pipeline
		// start with a stream of all students
		// add intermediate operations: filter, sorted, map to experience
		// terminal operation: forEach to print all elements
		// put peek operations before all other operations
		System.out.println("\n--- 15: insert peek operations in a pipeline");
		Data.students.stream()
			.peek(student -> System.out.printf("- before filter: %s%n", student))
			.filter(student -> student.getExperience() > 7)
			.peek(student -> System.out.printf("  after filter: %s%n", student))
			.sorted(Comparator.comparing(Student::getName))
			.peek(student -> System.out.printf("  sorted: %s%n", student))
			.mapToInt(Student::getExperience)
			.peek(experience -> System.out.printf("  mapped: %d%n", experience))
			.forEach(System.out::println);
	}

	// step 9: create static method with signature of BiConsumer.accept
	private static void methodObserver(Student student, String message) {
		System.out.printf("%s wants you to know that (s)he %s%n", student, message);
//		throw new RuntimeException("bang!");
	}
}
