// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice3;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;

public class Practice3 {

	public static void main(String[] args) {
		
		// step 1: sort students by name using anonymous inner class
		Collections.sort(Data.students, new Comparator<Student>() {
			@Override
			public int compare(Student s1, Student s2) {
				return s1.getName().compareTo(s2.getName());
			}
		});
		print("1: sort by name using anonymous inner class", Data.students);
		
		// step 2: sort students by experience using anonymous inner class
		Collections.sort(Data.students, new Comparator<Student>() {
			@Override
			public int compare(Student s1, Student s2) {
				return s1.getExperience() - s2.getExperience();
			}
		});
		print("2: sort by experience using anonymous inner class", Data.students);
		
		// step 3: sort students by name using lambda expression
		Collections.sort(Data.students, (Student s1, Student s2) -> s1.getName().compareTo(s2.getName()));
		print("3: sort by name with lambda expression", Data.students);
		
		// step 4: sort students by experience using lambda expression
		Collections.sort(Data.students, (Student s1, Student s2) -> s1.getExperience() - s2.getExperience());
		print("4: sort by experience using lambda expression", Data.students);
		
		// step 6: sort students by name using static method reference
		Collections.sort(Data.students, Student::compareByName);
		print("6: sort by name using static method reference", Data.students);
		
		// step 8: sort students by experience using static method reference
		Collections.sort(Data.students, Student::compareByExperience);
		print("8: sort by experience using static method reference", Data.students);
				
		// step 10: sort students by name using instance method reference
		Collections.sort(Data.students, Student::compareToName);
		print("10: sort by name using instance method reference", Data.students);
		
		// step 12: sort students by experience using instance method reference
		Collections.sort(Data.students, Student::compareToExperience);
		print("12: sort by experience using instance method reference", Data.students);
		
		// step 13: sort students by experience and then reversed by name using method references
		// why do we require the explicit target type information?
		Comparator<Student> experienceComparator = Student::compareToExperience;
		Comparator<Student> nameComparator = Student::compareToName;
		Collections.sort(Data.students, experienceComparator.thenComparing(nameComparator.reversed()));
		print("13: sort by experience and then reversed by name using method references", Data.students);
				
		// step 14: sort students by experience and then reversed by name using key extractor
		// why do we NOT require the explicit target type information?
		Collections.sort(Data.students, Comparator.comparing(Student::getExperience).thenComparing(Comparator.comparing(Student::getName).reversed()));
		print("14: sort by experience and then reversed by name using key extractors", Data.students);
				
		// step 16: rewrite the previous tests using the "execute around" test method
		test("16-2: sort by name with anonymous inner class and consumer", list -> Collections.sort(list, new Comparator<Student>() {
			@Override
			public int compare(Student s1, Student s2) {
				return s1.getName().compareTo(s2.getName());
			}
		}));
		test("16-3: sort by experience with anonymous inner class and consumer", list -> Collections.sort(list, new Comparator<Student>() {
			@Override
			public int compare(Student s1, Student s2) {
				return s1.getExperience() - s2.getExperience();
			}
		}));
		test("16-4: sort by name with lambda expression and consumer", 
				list -> Collections.sort(list, (s1, s2) -> s1.getName().compareTo(s2.getName())));
		test("16-5: sort by experience with lambda expression and consumer", 
				list -> Collections.sort(list, (s1, s2) -> s1.getExperience() - s2.getExperience()));
		test("16-7: sort by name using static method reference and consumer", 
				list -> Collections.sort(list, Student::compareByName));
		test("16-9: sort by experience using static method reference and consumer", 
				list -> Collections.sort(list, Student::compareByExperience));
		test("16-11: sort by name using instance method reference and consumer", 
				list -> Collections.sort(list, Student::compareToName));
		test("16-13: sort by experience using instance method reference and consumer", 
				list -> Collections.sort(list, Student::compareToExperience));
		test("16-14: sort by experience and then reversed by name using method references and consumer", 
				list -> {
					Comparator<Student> experienceComp = Student::compareToExperience;
					Comparator<Student> nameComp = Student::compareToName;
					Collections.sort(list, experienceComp.thenComparing(nameComp.reversed()));
				});
		test("16-15: sort by experience and then reversed by name using key extractors and consumer", 
				list -> Collections.sort(list, Comparator.comparing(Student::getExperience).thenComparing(Comparator.comparing(Student::getName).reversed())));
	}

    private static void print(String comment, List<? extends Object> list) {
        System.out.printf("%n--- %s%n", comment);
        System.out.println(list);
    }

    // step 15: implement the "execute around" pattern in a central test method
    // args: String comment, Consumer of student list
    // create a fresh list of students for each test
    private static void test(String comment, Consumer<List<Student>> tester) {
    	// before the test
        System.out.printf("%n--- %s%n", comment);
        List<Student> students = Arrays.asList(
        		new Student("Theresa May", 5),
        		new Student("Donald Trump", 6),
        		new Student("Donald Tusk", 6),
        		new Student("Kim Jong-un", 6),
        		new Student("Vladimir Putin", 4)
        );
        // test
    	tester.accept(students);
    	// after the test
        System.out.println(students);
   }
}
