// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice10;

import java.util.List;
import java.util.function.BiConsumer;

public interface Observable<T> {
	
	public default void addObserver(BiConsumer<T, String> consumer) {
		getObservers().add(consumer);
	}
	
	@SuppressWarnings("unchecked")
	public default void notifyObservers(String message) {
		getObservers().forEach(observer -> observer.accept((T) this, message));
	}
	
	public List<BiConsumer<T, String>> getObservers();
	
	public default void log(String message) {
		System.out.println("Observable: " + message);
	}
}
