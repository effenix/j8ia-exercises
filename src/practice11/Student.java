// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice11;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;

public class Student implements Observable<Student> {
	
    private String name;
    private int experience;
    private Teacher teacher;
    List<BiConsumer<Student, String>> observers = new ArrayList<>();
    Optional<Exam> exam = Optional.empty();
    

    public Student(String name, int experience) {
        this.name = name;
        this.experience = experience;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
		teacher.addStudent(this);
	}

    @Override
    public String toString() {
    	return String.format("%s(%s, %d)", getClass().getSimpleName(), name, experience);
    }
    
    public static int compareByName(Student s1, Student s2) {
		return s1.getName().compareTo(s2.getName());
    }
    
    public static int compareByExperience(Student s1, Student s2) {
		return s1.getExperience() - s2.getExperience();
    }
    
    public int compareToName(Student s2) {
		return name.compareTo(s2.getName());
    }
    
    public int compareToExperience(Student s2) {
		return experience - s2.getExperience();
    }

	@Override
	public List<BiConsumer<Student, String>> getObservers() {
		return observers;
	}

	public void learn(String task) {
		experience++;
		notifyObservers("learned " + task);
	}

	public void observed(String message) {
		System.out.printf("%s finally %s%n", name, message);
	}

	public Optional<Exam> getExam() {
		return exam;
	}

	public void setExam(Exam exam) {
		this.exam = Optional.of(exam);
	}
	
	// step 1: add method takeExam
	// arg: Exam
	// return: boolean true if passed, false if failed
	// notify observers that the student started taking the exam
	// wait a random time taking the exam (max 5 sec)
	// there's a 50% chance for a pass
	// if passed, tell the exam by calling its pass method
	public boolean takeExam(Exam exam) {
		notifyObservers(String.format("%s started exam %s", name, exam.getSubject()));
		randomDelay(5);
		boolean passed = new Random().nextBoolean();
		if (passed) {
			exam.pass(this);
		}
		return passed;
	}
	
	private void randomDelay(int seconds) {
		try {
			Thread.sleep(new Random().nextInt(seconds * 1000));
		} 
		catch (InterruptedException e) {
		}
	}
	
	// step 4: add method takeExamAsync
	// arg: Exam
	// return CompletableFuture<Boolean>
	// call takeExam in a thread
	// if it's done, complete the returned future
	public CompletableFuture<Boolean> takeExamAsync(Exam exam) {
//		CompletableFuture<Boolean> futurePassed = new CompletableFuture<>();
//		Thread thread = new Thread(() -> {
//			boolean passed = takeExam(exam);
//			futurePassed.complete(passed);
//		});
//		thread.start();
		return CompletableFuture.supplyAsync(() -> takeExam(exam));
	}
	
	// step 8: add method study, and also async version
	// return this student when finished
	// notify observers that the student began studying
	// wait a random time studying (max 5 sec)
	// notify observers that the student finished studying
	public Student study() {
		notifyObservers(String.format("%s began studying", name));
		randomDelay(5);
		notifyObservers(String.format("%s finished studying", name));
		return this;
	}

	public CompletableFuture<Student> studyAsync() {
		return CompletableFuture.supplyAsync(() -> study());
	}
}
