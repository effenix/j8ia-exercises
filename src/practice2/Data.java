// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice2;

import java.util.Arrays;
import java.util.List;

//step 2: add class Data
//- fields: static list with students for testing purposes

public class Data {

	static List<Student> students = Arrays.asList(
		new Student("John", 8),
		new Student("Paul", 9),
		new Student("George", 7),
		new Student("Ringo", 5),
		new Student("George Martin", 9)
	);

}
