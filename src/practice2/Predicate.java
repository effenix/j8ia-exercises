// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice2;

// step 21 (bonus):
// - Create generic interface Predicate
// - method: test

interface Predicate<T> {
    boolean test(T t);
}
