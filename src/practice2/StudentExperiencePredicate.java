// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice2;

// step 12:
// - Create class StudentExperiencePredicate
// - implements StudentPredicate
public class StudentExperiencePredicate implements StudentPredicate {

    @Override
    public boolean test(Student student) {
        return student.getExperience() > 7;
    }
    
}
