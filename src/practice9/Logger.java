// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice9;

// step 16: add interface Logger
// default method: log(String)
public interface Logger {
	public default void log(String message) {
		System.out.println("Logger: " + message);
	}
	
	// step 20: add static method logMessage(String)
	public static void logMessage(String message) {
		System.out.println("Logger: " + message);
	}
}
