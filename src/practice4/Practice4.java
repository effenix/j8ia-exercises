// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice4;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Practice4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// step 1: use imperative style code to print all students with experience over 7
		System.out.println("\n--- 1: use imperative style code to print all students with experience over 7");
		for (Student student: Data.students) {
			if (student.getExperience() > 7) {
				System.out.println(student);
			}
		}
		
		// step 2: use iterator to print all students with experience over 7
		System.out.println("\n--- 2: use iterator to print all students with experience over 7");
		for (Iterator<Student> it = Data.students.listIterator(); it.hasNext(); ) {
			Student student = it.next();
			if (student.getExperience() > 7) {
				System.out.println(student);
			}
		}
		
		// step 3: use streams without method chaining to print all students with experience over 7
		// use a Predicate and a Consumer
		System.out.println("\n--- 3: use streams without method chaining to print all students with experience over 7");
		Stream<Student> stream = Data.students.stream();
		stream = stream.filter(student -> student.getExperience() > 7);
		stream.forEach(student -> System.out.println(student));
		
		// step 4: use pipeline of streams to print all students with experience over 7
		System.out.println("\n--- 4: use pipeline to print all students with experience over 7");
		Data.students.stream()
			.filter(student -> student.getExperience() > 7)
			.forEach(student -> System.out.println(student));
		
		// step 5: count all students with first name George
		System.out.println("\n--- 5: count all students with first name George");
		long count = Data.students.stream()
			.filter(student -> student.getName().startsWith("George"))
			.count();
		System.out.println(count + " Georges");
		
		// step 6: print all students sorted by experience
		// use different types of method references
		System.out.println("\n--- 6: print all students sorted by experience");
		Data.students.stream()
			.sorted(Student::compareByExperience)
			.forEach(System.out::println);
		
		// step 7: create a list with the names of the 2 best students
		System.out.println("\n--- 7: create a list with the names of the 2 best students");
		List<String> names = Data.students.stream()
			.sorted(Comparator.comparing(Student::getExperience).reversed())
			.limit(2)
			.map(student -> student.getName())
			.collect(Collectors.toList());
		System.out.println(names);
		
		// step 13: print each student and their teacher, names only
		System.out.println("\n--- 13: print each student and their teacher, names only");
		Data.students
			.forEach(student -> System.out.printf("%s is learning a lot from %s%n",
					student.getName(), student.getTeacher().getName()));
		
		// step 14: create a list of the teachers of all students
		// each teacher should be mentioned only once
		System.out.println("\n--- 14: create a list of the teachers of all students");
		List<Teacher> teacherList = Data.students.stream()
				.map(student -> student.getTeacher())
				.distinct()
				.collect(Collectors.toList());
		System.out.println(teacherList);
	}

}
