// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice9;

import java.util.List;
import java.util.function.BiConsumer;

public interface Observable<T> {
	
    // step 2: implement default Observable method 
    // add consumer to list
	// problem: how to get the list?
	public default void addObserver(BiConsumer<T, String> consumer) {
		getObservers().add(consumer);
	}
	
	// step 3: add default notifyObservers method
	// for each observers, call the consumers method
	@SuppressWarnings("unchecked")
	public default void notifyObservers(String message) {
		getObservers().forEach(observer -> observer.accept((T) this, message));
	}
	
	// step 4: add abstract method to get the list of observers
	public List<BiConsumer<T, String>> getObservers();
	
	// step 14: add default method to log a message
	public default void log(String message) {
		System.out.println("Observable: " + message);
	}
}
