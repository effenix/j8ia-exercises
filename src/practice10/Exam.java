// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice10;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.function.BiConsumer;

public class Exam implements Observable<Exam> {
	private String subject;
    List<BiConsumer<Exam, String>> observers = new ArrayList<>();
    // step 1: add optional LocalDate field finalDate
    // include getter
    Optional<LocalDate> finalDate = Optional.empty();

	public Exam(String subject) {
		this.subject = subject;
	}
	
	// step 3: add constructor with extra arg finalDate
	public Exam(String subject, LocalDate finalDate) {
		this(subject);
		this.finalDate = Optional.of(finalDate);
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Override
	public String toString() {
    	return String.format("%s(%s)", getClass().getSimpleName(), subject);
	}
	
	@Override
	public List<BiConsumer<Exam, String>> getObservers() {
		return observers;
	}
	
	public void pass(Student student) {
		// step 4: check final date using get()
		// hint: use LocalDate.now()
//		if (finalDate.get().isBefore(LocalDate.now())) {
//			System.out.println("The exam is no longer available");
//			return;
//		}
		
		// step 7: check final date using isPresent()
//		if (finalDate.isPresent() && finalDate.get().isBefore(LocalDate.now())) {
//			System.out.println("The exam is no longer available");
//			return;
//		}
		
		// step 8: check final date using ifPresent()
//		finalDate.ifPresent( date -> {
//			if (date.isBefore(LocalDate.now())) {
//				System.out.println("The exam is no longer available");
//				// return won't work
//				// fix it
//				return;
//			}
//		});
		
		// step 9: check final date using orElseThrow()
		try {
			LocalDate date = finalDate.orElseThrow(NoSuchElementException::new);
			if (date.isBefore(LocalDate.now())) {
				System.out.printf("The exam \"%s\" is no longer available%n", subject);
				return;
			}
		}
		catch (NoSuchElementException nse) {
		}

		// step 12: set this exam for the student
		student.setExam(this);
		
		notifyObservers(student.getName() + " passed exam " + subject);
	}

	public Optional<LocalDate> getFinalDate() {
		return finalDate;
	}
	
	
}
