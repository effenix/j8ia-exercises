// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice9;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

// step 1: add class Exam
// fields: String subject
// add constructor, setter, getter, toString
// implement Observable
// do not yet implement its abstract methods

// step 17: implement Logger interface
// what's wrong?
public class Exam implements Observable<Exam>, Logger {
	private String subject;
    // step 7: add empty list of Consumers, just like in Student
	// question: why won't we move this to a common superclass of Student and Exam?
    List<BiConsumer<Exam, String>> observers = new ArrayList<>();

	public Exam(String subject) {
		this.subject = subject;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Override
	public String toString() {
    	return String.format("%s(%s)", getClass().getSimpleName(), subject);
	}
	
	// step 8: implement the getObservers method of the Observable interface
	@Override
	public List<BiConsumer<Exam, String>> getObservers() {
		return observers;
	}
	
	// step 10: add method "pass"
	// arg: Student
	// notify observers
	public void pass(Student student) {
		notifyObservers(student.getName() + " passed exam " + subject);
	}
	
	// step 18: override log method to fix the problem
	@Override
	public void log(String message) {
		System.out.println("Exam: " + message);
	}
}
