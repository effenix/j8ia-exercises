// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice4;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Data {
	
	static List<Student> students = Arrays.asList(
		new Student("John", 8),
		new Student("Paul", 9),
		new Student("George", 7),
		new Student("Ringo", 5),
		new Student("George Martin", 9)
	);

	// step 11: create static list of Teachers for testing purposes
    static List<Teacher> teachers = Arrays.asList(
    	new Teacher("Fred", "Java Acht in actie"),
    	new Teacher("David", "Java Huit en Action")
    );

	// step 12: give all students a random teacher
	static {
		Random random = new Random();
		students.forEach(student -> student.setTeacher(teachers.get(random.nextInt(teachers.size()))));
	}

}
