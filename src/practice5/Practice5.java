// Copyright 2019, Fred Zuijdendorp, effenix
//
// Practices for "Java 8 in Action" course
// Author: Fred Zuijdendorp, fred@effenix.nl

package practice5;

import java.time.LocalTime;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Practice5 {

	public static void main(String[] args) {
		
		// step 4: print all students of all teachers
		// 1st try, using a single map to map a teacher to a list of students
		System.out.println("\n--- 4: all students of all teachers, as lists");
		Data.teachers.stream()
			.map(teacher -> teacher.getStudents())
			.forEach(System.out::println);

		// step 5: print all students of all teachers
		// 2nd try, using 2 maps to map a teacher to a stream of students
		System.out.println("\n--- 5: all students of all teachers, as streams");
		Data.teachers.stream()
			.map(teacher -> teacher.getStudents())
			.map(List::stream)
			.forEach(System.out::println);

		// step 6: print all students of all teachers
		// 3rd try, using flatMap to map a teacher to individual students
		System.out.println("\n--- 6: all students of all teachers, as students");
		Data.teachers.stream()
			.map(teacher -> teacher.getStudents())
			.flatMap(List::stream)
			.forEach(System.out::println);

		// step 7: test if some students have a less than sufficient experience
		System.out.println("\n--- 7: some students have a less than sufficient experience");
		System.out.printf("%b%n", Data.students.stream().anyMatch(student -> student.getExperience() < 6));

		// step 8: test if all students have a teacher
		System.out.println("\n--- 8: all students have a teacher");
		System.out.printf("%b%n", Data.students.stream().allMatch(student -> student.getTeacher() != null));

		// step 9: test if no student is perfect
		// store the predicate in a variable for later re-use
		System.out.println("\n--- 9: no student is perfect");
		Predicate<Student> perfect = student -> student.getExperience() == 10;
		System.out.printf("%b%n", Data.students.stream().noneMatch(perfect));

		// step 10: find any student that is perfect
		System.out.println("\n--- 10: find any student that is perfect");
		Optional<Student> mrPerfect = Data.students.stream()
			.filter(perfect)
			.findAny();
		System.out.println(mrPerfect);

		// step 11: find any student that is good enough
		// store the predicate in a variable for later re-use
		// what happens if you repeat the test multiple times?
		System.out.println("\n--- 11: find any student that is good enough");
		Predicate<Student> goodEnough = student -> student.getExperience() >= 7;
		Data.students.stream()
			.filter(goodEnough)
			.findAny()
			.ifPresent(System.out::println);

		// step 12: find the first student that is good enough
		// is it the same student as the one found in step 11?
		System.out.println("\n--- 12: find the first student that is good enough");
		Data.students.stream()
			.filter(goodEnough)
			.findFirst()
			.ifPresent(System.out::println);

		// step 13: find the lowest experience student that is good enough
		System.out.println("\n--- 13: find the lowest experience student that is good enough");
		Data.students.stream()
			.filter(goodEnough)
			.sorted(Comparator.comparing(Student::getExperience))
			.findFirst()
			.ifPresent(System.out::println);

		// step 14: for each teacher, count the number of students
		// 1st try: do not use streams
		System.out.println("\n--- 14: for each teacher, count the number of students, not using streams");
		Data.teachers.forEach(teacher -> System.out.printf("%s has %d students%n", teacher.getName(), teacher.getStudents().size()));

		// step 15: for each teacher, count the number of students
		// 2nd try: use streams and simple operations
		// is this a better solution than step 14?
		System.out.println("\n--- 15: for each teacher, count the number of students, using streams");
		Data.teachers.stream()
			.forEach(teacher -> {
				long count = teacher.getStudents().stream().count();
				System.out.printf("%s has %d students%n", teacher.getName(), count);
			});

		// step 16: for each teacher, count the number of students
		// 3rd try: use streams and reduce
		// try the same with the sum operation
		System.out.println("\n--- 16: for each teacher, count the number of students, using reduce");
		Data.teachers.stream()
			.forEach(teacher -> {
				int count = teacher.getStudents().stream().mapToInt(student -> 1).reduce(0, (a, b) -> a + b);
				System.out.printf("%s has %d students%n", teacher.getName(), count);
			});
		
		// step 17: for each teacher, find the best student
		// 1st try: use max() and Optional
		System.out.println("\n--- 17: for each teacher, find the best student");
		Data.teachers.forEach(teacher -> {
			teacher.getStudents().stream()
				.max(Comparator.comparing(Student::getExperience))
				.ifPresent(student -> System.out.printf("%s's best student is %s%n", student.getTeacher().getName(), student.getName()));
		});
		
		// step 18: join the strings in an array
		System.out.println("\n--- 18: join the strings in an array");
		String[] days = {"Tuesday", "Wednesday", "Friday"};
		String courseDays = Arrays.stream(days).collect(Collectors.joining(" and "));
		System.out.println(courseDays);
		
		// step 19: print the sum of 1..10
		// use the sum operation
		System.out.println("\n--- 19: print the sum of 1..10, using sum()");
		System.out.println(IntStream.rangeClosed(1, 10).sum());
		
		// step 20: print the sum of 1..10
		// use the reduce operation
		System.out.println("\n--- 20: print the sum of 1..10, using reduce()");
		System.out.println(IntStream.rangeClosed(1, 10).reduce(0, Integer::sum));
		
		// step 21: print the product of 1..10
		System.out.println("\n--- 21: print the product of 1..10");
		System.out.println(IntStream.rangeClosed(1, 10).reduce(1, (x, y) -> x * y));
		
		// step 22: generate a stream of LocalTime objects
		// use the factory method now()
		System.out.println("\n--- 22: generate a stream of LocalTime objects");
		Stream.generate(LocalTime::now).limit(10).forEach(System.out::println);
	}
}
